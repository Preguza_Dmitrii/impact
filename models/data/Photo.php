<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property int $id
 * @property string $title
 * @property string $file_name
 * @property int $size
 * @property int $product_id
 * @property string $created_at
 *
 * @property Product $product
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'file_name', 'size'], 'required'],
            [['size', 'product_id'], 'integer'],
            [['created_at'], 'safe'],
            [['title', 'file_name'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'file_name' => 'File Name',
            'size' => 'Size',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
