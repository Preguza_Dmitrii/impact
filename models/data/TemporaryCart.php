<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "temporary_cart".
 *
 * @property int $id
 * @property int $product_id
 * @property int|null $Quantity
 * @property int|null $user_id
 * @property string|null $guest_id
 * @property string $created_at
 * @property string|null $updated_at
 */
class TemporaryCart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temporary_cart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'Quantity', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['guest_id'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'Quantity' => 'Quantity',
            'user_id' => 'User ID',
            'guest_id' => 'Guest ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
