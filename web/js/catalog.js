$(document).ready(function() {

    $('.quick-view').click(function () {
        var button = $(this);
        $('#productModal').modal('show');
        var id = button.attr("data-id");
        $.ajax({
            url: "/product/info/"+id,
            type: 'GET',
            processData: false,
            contentType: false,
            // cache: false,
            data: id,
            beforeSend: function () {
                $(".some-catalog-content").html('<img src="/images/products/loading.gif" alt="Loading" style="margin: 0 auto;">');
            },
            success: function (res) {
                $(".some-catalog-content").html(res);
                console.log('good');

            },
            // error: function (res) {
            //     alert('error');
            // }
        });
        return false;
    });

    //Add product to Cart
    $('.add-to-cart').click(function () {
        var btn = $(this);
        var id = btn.attr("data-id");
        $.ajax({
            url: "/cart/add/"+id,
            type: 'GET',
            processData: false,
            contentType: false,
            beforeSend: function () {
                btn.html('<img src="/images/products/loading.gif" alt="Loading" style="margin: 0 auto;">');
            },
            success: function (res) {
                btn.add('#added-to-cart');
                btn.html('');
                btn.html('<span class="ti-shopping-cart">&#x2714;</span>');
            },
        });
        return false;
    });

    //Add product to Mini Cart
    $('.cart__menu').click(function () {
        $.ajax({
            url: "/cart/mini-cart/",
            type: 'GET',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $(".mini-cart").html('<img src="/images/products/loading.gif" alt="Loading" style="margin: 0 auto;">');
            },
            success: function (res) {
                $(".mini-cart").html(res);
            },
        });
        return false;
    });

    //Add product to Mini Cart
    $('.remove-cart').click(function () {
        var btn = $(this);
        var id = btn.attr("data-id");
        $.ajax({
            url: "/cart/delete/"+id,
            type: 'GET',
            data: id,
            processData: false,
            contentType: false,
            success: function (res) {
                btn.parent().parent().remove();
                // $("#mini-cart").html(res);
            },
        });
        return false;
    });

    //Delete product at Mini Cart
    // $('a.mini-cart-delete').click(function () {
    //     alert('id');
    //     // var mybtn = $(this);
    //     // var id = mybtn.attr("data-cart-id");
    //     // alert(id);
    //     // $.ajax({
    //     //     url: "cart/mini-cart-delete/",
    //     //     type: 'GET',
    //     //     data: id,
    //     //     processData: false,
    //     //     contentType: false,
    //     //     // beforeSend: function () {
    //     //     //     $("#mini-cart").html('<img src="/images/products/loading.gif" alt="Loading" style="margin: 0 auto;">');
    //     //     // },
    //     //     success: function (res) {
    //     //         alert('res');
    //     //         // $("#mini-cart").html(res);
    //     //     },
    //     // });
    //     return false;
    // });

    $('input.cart-number').on('keyup mouseup keydown change blur', function() {
        var value = $(this).val();
        var price = $(this).parent().parent().find('.product-price').text();
        $(this).parent().parent().find('.product-subtotal').text((price * value).toFixed(2));
    });
});