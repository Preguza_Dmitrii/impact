<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'layout' => 'catalog',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'enableCookieValidation' => true,
            'cookieValidationKey' => '123fdfdfvfdgsdgffdgrt2323232',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'preguzadmitrii@gmail.com',
                'password' => 'PDA291099',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // API rules
                //Categories
                [
                    'pattern' => 'api/categories/<id:\d+>',
                    'route' => 'api/categories',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/categories/update/<id:\d+>',
                    'route' => 'api/categories/update',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/categories/delete/<id:\d+>',
                    'route' => 'api/categories/delete',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/categories/create',
                    'route' => 'api/categories/create',
                    'suffix' => '',
                ],
                //product
                [
                    'pattern' => 'api/product/<id:\d+>',
                    'route' => 'api/product',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/product/update/<id:\d+>',
                    'route' => 'api/product/update',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/product/delete/<id:\d+>',
                    'route' => 'api/product/delete',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/product/create',
                    'route' => 'api/product/create',
                    'suffix' => '',
                ],
                //Photo
                [
                    'pattern' => 'api/photo/<id:\d+>',
                    'route' => 'api/photo',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/photo/delete/<id:\d+>',
                    'route' => 'api/photo/delete',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/photo/create',
                    'route' => 'api/photo/create',
                    'suffix' => '',
                ],
                //ProductCategories
                [
                    'pattern' => 'api/product-categories/<id:\d+>',
                    'route' => 'api/product-categories',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/product-categories/delete/<id:\d+>',
                    'route' => 'api/product-categories/delete',
                    'suffix' => '',
                ],
                [
                    'pattern' => 'api/product-categories/create',
                    'route' => 'api/product-categories/create',
                    'suffix' => '',
                ],

                '<controller:\w+>/<action:.+>/<id:\d+>' => '<controller>/<action>',
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/product', 'pluralize'=>false],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/categories'],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/photo', 'pluralize'=>false],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/product-categories'],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/rest/product', 'pluralize'=>false],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/rest/categories'],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/rest/photo', 'pluralize'=>false],
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'api/rest/product-categories'],

            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
