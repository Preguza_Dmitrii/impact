<?php


namespace app\controllers\admin;


use app\controllers\AppController;
use app\models\data\Categories;
use app\models\data\Photo;
use Yii;
use yii\base\BaseObject;
use yii\data\Pagination;
use yii\web\Controller;

class CategoriesController extends AppController
{
    public $layout = 'admin';

    public function actionIndex()
    {
        $category = new Categories;
        $categories = $category::find();
        $countQuery = $categories;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 5,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);
        $pages->route = '/admin/categories';
        $models = $categories->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $category = new Categories;
        return $this->render('category', [
            'categories' => $models,
            'category' => $category,
            'pages' => $pages,
        ]);

    }
    public function actionUpdateCategory($id)
    {
        echo $id;
    }
    public function actionCreate()
    {
        $category = new Categories();
        $category->name = $_POST['Categories']['name'];
        $category->save();

        return $this->redirect('/admin/categories');

    }
    public function actionUpdate($id)
    {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $name = Yii::$app->request->post()['Categories']['name'];
            $model = $model::findOne($id);
            $model->name = $name;
            $model->save();
            return $this->redirect('/admin/categories');
        }

        $id = Categories::find()
            ->select('name')
            ->where(['id' => $id])
            ->all();
        $name = $id[0]->name;

        return $this->render('update', ['name' => $name, 'model' => $model]);
    }
    public function actionDelete($id)
    {
        Categories::findOne($id)->delete();

        return $this->redirect('/admin/categories');
    }
}