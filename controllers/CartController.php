<?php


namespace app\controllers;


use app\models\data\Cart;
use app\models\data\Photo;
use app\models\data\Product;
use app\models\data\TemporaryCart;
use Yii;

class CartController extends AppController
{
    public $layout = 'cart';

    public function actionIndex()
    {
        $guest_id = Yii::$app->getRequest()->getCookies()->getValue('guest_id') ?: null;
        $user_id = Yii::$app->user->identity ?:null;

        $carts = TemporaryCart::find()->where(['guest_id' => $guest_id, 'user_id' => $user_id])->all();
        if(!empty($carts)) {
            $cart = [];
            foreach ($carts as $item) {
                $value['cart_id'] = $item['id'];
                $value['Quantity'] = $item['Quantity'] ? $item['Quantity'] : '1';
                $value['product'] = Product::find()->where(['id' => $item['product_id']])->asArray()->one();
                $value['productTotalPrice'] = $value['product']['price'] * $value['Quantity'];
                $total += $value['product']['price'] * $value['Quantity'];
                $value['photo'] = Photo::find()->select('title')->where(['product_id' => $item['product_id']])->asArray()->one();
                $cart[] = $value;
            }
        }
        $cart['total'] = $total;

        return $this->render('index', ['cart' => $cart]);
    }
    public function actionAdd($id)
    {
        $guest_id = Yii::$app->getRequest()->getCookies()->getValue('guest_id') ?: null;
        $user_id = Yii::$app->user->identity ?:null;
        if(Product::findOne($id)) {
            if(TemporaryCart::find()->where(['product_id' => $id, 'guest_id' => $guest_id, 'user_id' => $user_id])->all()) {
                return 'Данный продукт уже был добавлен в карзину!';
            }else {
                $cart = new TemporaryCart();
                $cart->product_id = $id;
                $cart->Quantity = 1;
                $cart->guest_id = $guest_id;
                $cart->user_id = Yii::$app->getRequest()->getCookies()->getValue('user_token');
//$this->dd($cart->user_id);exit;
                $cart->save();
                return 'Продукт успешно добавлен в карзину!';
            }
        }
    }

    public function actionMiniCart()
    {
        $userToken = Yii::$app->getRequest()->getCookies()->getValue('user_token');
        $guest_id = Yii::$app->getRequest()->getCookies()->getValue('guest_id');

        if(!$userToken) {
            $carts = TemporaryCart::find()->where(['guest_id' => $guest_id])->all();
        }else {
            $carts = TemporaryCart::find()->where(['user_id' => $userToken])->all();
        }

        if(!empty($carts)){
            $cart = [];
            foreach ($carts as $item) {
                $value['cart_id'] = $item['id'];
                $value['Quantity'] = $item['Quantity']?$item['Quantity']:'1';
                $value['product'] = Product::find()->where(['id' => $item['product_id']])->asArray()->one();
                $total += $value['product']['price'] * $value['Quantity'];
                $value['photo'] = Photo::find()->select('title')->where(['product_id' => $item['product_id']])->asArray()->one();
                $cart[] = $value;
            }
            $cart['total'] = $total;

            $path = '/web/uploads/';
            $customImg = '/images/products/placeholderProduct.png';

            foreach ($cart as $item) {
//                $this->dd($item);exit;
                if($item == $cart['total']) {
                    continue;
                }
                $pathFull = $item['photo'] ? $path.$item['photo']['title'] : $customImg;

                $product .= "<div class='shp__single__product'>
        <div class='shp__pro__thumb'>
            <a href='#'>
                <img src='".$pathFull."' alt='product images'>
            </a>
        </div>
        <div class='shp__pro__details'>
            <h2><a href='/product?id=".$item['product']['id']."'>".$item['product']['name']."</a></h2>
<span class='quantity'>Кол-во: ".$item['Quantity']." </span>
<span class='shp__price'>Цена: ".$item['product']['price']." </span>
</div>
<div class='remove__btn'>
    <a href='#' data-cart-id='".$item['cart_id']."' title='Remove this item' class='mini-cart-delete'><i class='zmdi zmdi-close'></i></a>
</div>
</div>";
            }

            $html = "<div class='offsetmenu__close__btn'>
                        <a href='#'><i class='zmdi zmdi-close'></i></a>
            </div><div class='shp__cart__wrap'>".$product."</div>
<ul class='shoping__total'>
    <li class='subtotal'>Subtotal:</li>
    <li class='total__price'> ".$cart['total']." </li>
</ul>
<ul class='shopping__btn'>
    <li><a href='/cart'>View Cart</a></li>
    <li class='shp__checkout'><a href='/checkout'>Checkout</a></li>
</ul>";
        }else {
            $html = "<h3 class='text-primary'>Вы пока не добавили ни одного продукта!</h3>";
        }

        return $html;
    }
    public function actionMiniCartDelete()
    {
        return $_GET['id'] ;
    }
    public function actionUpdate()
    {
        $cart = new TemporaryCart();
        foreach (Yii::$app->request->post('cart_id') as $key => $id) {
            $cart = $cart::findOne($id);
            $cart->Quantity = Yii::$app->request->post('quantity')[$key];
//            $cart->Quantity = Yii::$app->request->post('quantity')[$key];
            $cart->save();
        }


        return $this->redirect('/cart');
    }
    public function actionDelete($id)
    {
        TemporaryCart::findOne($id)->delete();
    }
}