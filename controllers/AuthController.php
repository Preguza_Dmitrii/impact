<?php


namespace app\controllers;


use app\models\data\Product;
use app\models\data\TemporaryCart;
use app\models\data\Users;
use app\models\SignUpForm;
use Yii;
use yii\web\Cookie;

class AuthController extends AppController
{
    public $layout = 'auth';

    public function actionIndex()
    {
        $model = new Users();
        return $this->render('index', ['model' => $model, 'errorLogin' => '']);
    }
    public function actionRegister()
    {
        $model = new Users();
        if ($model->load(Yii::$app->request->post())) {
            $hash = md5(trim(Yii::$app->request->post()['Users']['password']));
            $model->name = trim(Yii::$app->request->post()['Users']['name']);
            $model->email = trim(Yii::$app->request->post()['Users']['email']);
            $model->password = $hash;

            if ($model->validate()) {
                $model->save();

                $guest_id = Yii::$app->getRequest()->getCookies()->getValue('guest_id');
                $carts = Yii::$app->db->createCommand();
                $carts->update('temporary_cart', ['user_id' => $model->id], ['guest_id' => $guest_id]);

                $model = new Users();
                $error = '';
                return $this->render('index', ['model' => $model, 'errorLogin' => $error]);
            }
        }
        $model = new Users();
        $error = 'Введённые вами данные не подходят!';
        return $this->render('index', ['model' => $model, 'errorLogin' => $error]);
    }
    public function actionLogin()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $user = Users::findOne(['name' => trim($model->name)])) {
                $hash = $user->password;
                $password = $model->password;
                if(md5($password) == $hash) {
                    $cookie = new Cookie([
                        'name' => 'user_token',
                        'value' => $user->id,
                        'expire' => time() + 86400 * 365,
                    ]);
                    Yii::$app->getResponse()->getCookies()->add($cookie);
                    return $this->redirect('/');
                }
            $model = new Users();
            $error = 'Введённые вами данные не подходят!';
            return $this->render('index', ['model' => $model, 'errorLogin' => $error]);
        }
        $model = new Users();
        $error = 'Введённые вами данные не подходят!';
        return $this->render('index', ['model' => $model, 'errorLogin' => $error]);

    }
}