<?php

namespace app\controllers;

use app\models\data\Cart;
use app\models\data\Categories;
use app\models\data\Photo;
use app\models\data\Product;
use app\models\data\ProductCategories;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends AppController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $categories = Categories::find()->orderBy(['id' => SORT_DESC])->all();
        $categoriesArr = [];
        foreach ($categories as $category) {



            $item['id'] = $category->id;
            $item['name'] = $category->name;
            $item['count'] = count(ProductCategories::find()
                ->with('category')
                ->where(['category_id' => $category->id])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->all());

            $categoriesArr[] = $item;
        }

        $products = ProductCategories::find()
            ->with('product')
            ->groupBy('product_id')
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->all();

        $productsArr = [];
        foreach ($products as $product) {
            $item['id'] = $product['product']['id'];
            $item['name'] = $product['product']['name'];
            $item['info'] = $product['product']['info'];
            $item['price'] = $product['product']['price'];
            $item['photo'] = Photo::find()
                ->where(['product_id' => $product['product']['id']])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->all();

            $productsArr[] = $item;
        }


        return $this->render('index', ['categoriesArr' => $categoriesArr, 'productsArr' => $productsArr]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionProduct()
    {
        $model = new Product;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            var_dump(123);exit;
            return $this->render('../admin/product/product', ['model' => $model,'pages' => '', 'categories' => '']);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            var_dump(123);exit;
            return $this->render('/product', ['model' => $model,'pages' => '', 'categories' => '',]);
        }
    }


    public function actionCategoryFilter($id)
    {

        $products = ProductCategories::find()
            ->with('product')
            ->where(['category_id' => $id])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $productsArr = [];
        foreach ($products as $product) {
            $item['id'] = $product['product']->id;
            $item['name'] = $product['product']->name;
            $item['info'] = $product['product']->info;
            $item['price'] = $product['product']->price;
            $item['photo'] = Photo::find()
                ->where(['product_id' => $product['product']->id])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->all();

            $productsArr[] = $item;
        }


        $categories = Categories::find()->orderBy(['id' => SORT_DESC])->all();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $item['id'] = $category->id;
            $item['name'] = $category->name;
            $item['count'] = count(ProductCategories::find()
                ->with('category')
                ->where(['category_id' => $category->id])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->all());

            $categoriesArr[] = $item;
        }
        return $this->render('index', ['categoriesArr' => $categoriesArr,  'productsArr' => $productsArr]);
    }
}
