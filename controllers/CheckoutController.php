<?php


namespace app\controllers;


use app\models\data\Cart;
use app\models\data\Order;
use app\models\data\Product;
use app\models\data\TemporaryCart;
use Decimal\Decimal;
use Yii;
use yii\web\Cookie;

class CheckoutController extends AppController
{
    public $gmail = 'preguzadmitrii@gmail.com';
    public $pass = 'PDA291099';
    public $layout = 'checkout';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCheck()
    {
        $guest_id = Yii::$app->getRequest()->getCookies()->getValue('guest_id') ?: null;
        $user_id = Yii::$app->getRequest()->getCookies()->getValue('user_token') ?:null;

        $carts = TemporaryCart::find()->select(['id', 'product_id', 'Quantity'])->where(['user_id' => $user_id, 'guest_id' => $guest_id])->all();
        if(!empty($carts)) {
            if (Yii::$app->request->post()) {

                $firstName = Yii::$app->request->post('firstName');
                $lastName = Yii::$app->request->post('lastName');
                $address = Yii::$app->request->post('address');
                $email = Yii::$app->request->post('email');
                $phone = Yii::$app->request->post('phone');
                $message = Yii::$app->request->post('message');
                $order = new Order();

                foreach ($carts as $cart) {
                    $item['product'] = Product::findOne($cart['product_id']);
                    $trTable .= "
                            <tr>
                                <th>" . $item['product']->name . "</th>
                                <th>" . $cart['Quantity'] . "</th>
                                <th>" . $item['product']->price . "</th>
                                <th>" . $item['product']->price * $cart['Quantity'] . "</th>
                            </tr>";
                    $totalSum += $item['product']->price * $cart['Quantity'];

//                    if (Yii::$app->user->isGuest) {
//                        $order->user_id = $guest_id;
//                    } else {
//                        $order->user_id = $user_id;
//                    }

                    if (!$order->id) {
                        $order->guest_id = $guest_id;
                        $order->user_id = $user_id;
                        $order->name = trim($firstName . $lastName);
                        $order->phone = trim((string)$phone);
                        $order->email = trim($email);
                        $order->save();
                    }

//                $this->dd($order->id);exit;
                    $cartMain = new Cart();
                    $cartMain->order_id = $order->id;
                    $cartMain->product_id = $cart['product_id'];
                    $cartMain->quantity = $cart['Quantity'];
                    $cartMain->price = $item['product']->price;
                    $cartMain->save();
                    TemporaryCart::findOne($cart->id)->delete();
                }



                $htmlMessage = <<<HTML
            <h2>Заказ</h2>
            <table>
            <tr>
                <th>Имя продукта</th>
                <th>Количество</th>
                <th>Цена</th>
                <th>Суммарная Цена</th>
            </tr>
            $trTable
            </table>
            <p><b>Итоговая цена: </b>$totalSum</p>
            HTML;
                $htmlClient = <<<HTML
            <h2>Данный клиента</h2>
            <table>
            <tr>
                <th>Имя Фамилия</th>
                <th>Email</th>
                <th>Адрес</th>
                <th>Номер телефона</th>
                <th>Сообщение</th>
            </tr>
            <tr>
                <th>$firstName $lastName</th>
                <th>$email</th>
                <th>$address</th>
                <th>$phone</th>
                <th>$message</th>
            </tr>
            </table>
        HTML;

                $html = $htmlMessage . $htmlClient;

                $sendRes = \Yii::$app->mailer;
                $sendRes->setTransport([
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.gmail.com',
                    'username' => $this->gmail,
                    'password' => $this->pass,
                    'port' => '465',
                    'encryption' => 'ssl',
                ]);
                $sendRes = $sendRes->compose();
//        $this->dd($this->gmail);exit;
                $sendRes->setFrom($this->gmail)
                    ->setTo('dimaspr12345@gmail.com')
                    ->setSubject('Тема сообщения')
                    ->setHtmlBody($html)
                    ->send();
                return $this->redirect('/');
            }
            $session = Yii::$app->session;
            $session['checkError'] = 'Вы не заполнили форму!';
            return $this->render('index');
        }
        $session = Yii::$app->session;
        $session['checkError'] = 'Вы не добавили ни одного продукта!';
        return $this->render('index');
    }
}