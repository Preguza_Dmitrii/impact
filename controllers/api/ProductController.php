<?php


namespace app\controllers\api;

use app\controllers\AppController;
use app\models\data\Categories;
use app\models\data\Product;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;

class ProductController extends AppController
{


    public function actions()
    {
        $actions = parent::actions();
        unset(  $actions['update']);
        return $actions;
//        return [
//            'update' => [
//                'class' => $this,
//                'method' => $this->actionUpdate()
//            ],
//            'delete' => [
//                'class' => $this,
//                $this->actionDelete()
//            ],
////            'create' => $this->actionCreate(),
//        ];
    }
    public function init()
    {
        if(isset(Yii::$app->request->headers['accept'])) {
            switch (Yii::$app->request->headers['accept']) {
                case 'application/json': Yii::$app->response->format = Response::FORMAT_JSON;break;
                case 'application/xml': Yii::$app->response->format = Response::FORMAT_XML;break;
            }
        }elseif (isset(Yii::$app->request->headers['content-type'])) {
            switch (Yii::$app->request->headers['content-type']) {
                case 'application/json': Yii::$app->response->format = Response::FORMAT_JSON;break;
                case 'application/xml': Yii::$app->response->format = Response::FORMAT_XML;break;
                default: Yii::$app->response->format = Response::FORMAT_JSON;
            }
        }else {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        parent::init(); // TODO: Change the autogenerated stub
    }
    public function actionIndex($id = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        if(Yii::$app->request->get() && $id !== 0) {
            return Product::findOne($id);
        }else {
            return Product::find()->all();
        }
    }

    public function actionUpdate()
    {
        if (!empty(Yii::$app->request->post()) && $_GET['id']) {
            $products = new Product();

            $product = $products::findOne($_GET['id']);
            if (!empty($product)) {
                $product->name = Yii::$app->request->post("name");
                $product->info = Yii::$app->request->post("info");
                $product->price = Yii::$app->request->post("price");
                $product->save();
                return 'Вы успешно обновили продукт!';
            } else {
                return 'Такого продукта не существует!';
            }
        }
//        $this->dd($_GET['id']);exit;
    }

    public function actionCreate()
    {
        $product = new Product();
        if(!empty(Yii::$app->request->post())) {
            $name = Yii::$app->request->post('name');
            $info = Yii::$app->request->post('info');
            $price = Yii::$app->request->post('price');
            $product->name = $name;
            $product->info = $info?:null;
            $product->price = $price;
            $product->save();
            return 'Продукт успешно создан!';exit;
        }
    }
    public function actionDelete()
    {
        if(isset($_GET['id'])) {
            if (Product::findOne($_GET['id'])) {
                Product::findOne($_GET['id'])->delete();
                return 'Продукт успешно удален!';exit;
            }else {
                return 'Продукт не существует!';exit;
            }
        }
    }

}