<?php


namespace app\controllers\api\rest;

use yii\rest\ActiveController;

class ProductController extends ActiveController
{

    public $modelClass = 'app\models\data\Product';


}