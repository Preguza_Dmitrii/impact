<?php


namespace app\controllers\api\rest;

use yii\rest\ActiveController;

class PhotoController extends ActiveController
{
    public $modelClass = 'app\models\data\Photo';
}