<?php


namespace app\controllers\api\rest;

use yii\rest\ActiveController;

class ProductCategoriesController extends ActiveController
{
    public $modelClass = 'app\models\data\ProductCategories';
}