<?php


namespace app\controllers\api\rest;

use yii\rest\ActiveController;

class CategoriesController extends ActiveController
{
    public $modelClass = 'app\models\data\Categories';
}