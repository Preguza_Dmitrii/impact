<?php


namespace app\controllers;


use app\models\data\Photo;
use app\models\data\Product;

class ProductController extends AppController
{
    public $layout = 'product';

    public function actionIndex($id)
    {

        $product = Product::find()
            ->where(['id' => $id])
            ->all();

        $products = [];
        foreach ($product as $value) {
            $item['id'] = $value->id;
            $item['name'] = $value->name;
            $item['info'] = $value->info;
            $item['price'] = $value->price;
            $item['photo'] = Photo::find()
                ->where(['product_id' => $value->id])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->all();

            $products[] = $item;
        }
        return $this->render('index', ['products' => $products]);
    }
    public function actionInfo($id)
    {
        $product = Product::findOne($id);
        $photo = Photo::find()
            ->where(['product_id' => $id])
            ->all();
        return $this->renderPartial('info', ['product' => $product, 'photo' => $photo]);
    }
}