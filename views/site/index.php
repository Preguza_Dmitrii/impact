<div class="row">
    <p>Test</p>
    <p>Test</p>
    <p>Test</p>
    <p>Test</p>
    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
        <div class="htc__shop__left__sidebar">
            <!-- Start Range -->
            <div class="htc-grid-range">
                <h4 class="section-title-4">FILTER BY PRICE</h4>
                <div class="content-shopby">
                    <div class="price_filter s-filter clear">
                        <form action="#" method="GET">
                            <div id="slider-range"></div>
                            <div class="slider__range--output">
                                <div class="price__output--wrap">
                                    <div class="price--output">
                                        <span>Price :</span><input type="text" id="amount" readonly>
                                    </div>
                                    <div class="price--filter">
                                        <a href="#">Filter</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Range -->
            <!-- Start Product Cat -->
            <div class="htc__shop__cat">
                <h4 class="section-title-4">КАТЕГОРИИ ТОВАРОВ</h4>
                <ul class="sidebar__list">
                    <?php foreach ($categoriesArr as $category) : ?>
                        <li><a href="/site/category-filter/<?= $category['id'] ?>"><?= $category['name'] ?> <span><?= $category['count'] ?></span></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- End Product Cat -->
            <!-- Start Color Cat -->
            <div class="htc__shop__cat">
                <h4 class="section-title-4">CHOOSE COLOUR</h4>
                <ul class="sidebar__list">
                    <li class="black"><a href="#"><i class="zmdi zmdi-circle"></i>Black<span>3</span></a></li>
                    <li class="blue"><a href="#"><i class="zmdi zmdi-circle"></i>Blue <span>4</span></a></li>
                    <li class="brown"><a href="#"><i class="zmdi zmdi-circle"></i>Brown <span>3</span></a></li>
                    <li class="red"><a href="#"><i class="zmdi zmdi-circle"></i>Red <span>6</span></a></li>
                    <li class="orange"><a href="#"><i class="zmdi zmdi-circle"></i>Orange <span>10</span></a></li>
                </ul>
            </div>
            <!-- End Color Cat -->
            <!-- Start Size Cat -->
            <div class="htc__shop__cat">
                <h4 class="section-title-4">PRODUCT CATEGORIES</h4>
                <ul class="sidebar__list">
                    <li><a href="#">xl <span>3</span></a></li>
                    <li><a href="#">l <span>4</span></a></li>
                    <li><a href="#">lm <span>3</span></a></li>
                    <li><a href="#">ml <span>6</span></a></li>
                    <li><a href="#">m <span>10</span></a></li>
                    <li><a href="#">ml <span>3</span></a></li>
                </ul>
            </div>
            <!-- End Size Cat -->
            <!-- Start Tag Area -->
            <div class="htc__shop__cat">
                <h4 class="section-title-4">Tags</h4>
                <ul class="htc__tags">
                    <li><a href="#">All</a></li>
                    <li><a href="#">Clothing</a></li>
                    <li><a href="#">Kids</a></li>
                    <li><a href="#">Accessories</a></li>
                    <li><a href="#">Stationery</a></li>
                    <li><a href="#">Homelife</a></li>
                    <li><a href="#">Appliances</a></li>
                    <li><a href="#">Clothing</a></li>
                    <li><a href="#">Baby</a></li>
                    <li><a href="#">Beauty</a></li>
                </ul>
            </div>
            <!-- End Tag Area -->
        </div>
    </div>
    <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 smt-30">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="producy__view__container">
                    <!-- Start Short Form -->
                    <div class="product__list__option">
                        <div class="order-single-btn">
                            <select class="select-color selectpicker">
                                <option>Sort by newness</option>
                                <option>Match</option>
                                <option>Updated</option>
                                <option>Title</option>
                                <option>Category</option>
                                <option>Rating</option>
                            </select>
                        </div>
                        <div class="shp__pro__show">
                            <span>Showing 1 - 4 of 25 results</span>
                        </div>
                    </div>
                    <!-- End Short Form -->
                    <!-- Start List And Grid View -->
                    <ul class="view__mode" role="tablist">
                        <li role="presentation" class="grid-view active"><a href="#grid-view" role="tab" data-toggle="tab"><i class="zmdi zmdi-grid"></i></a></li>
                        <li role="presentation" class="list-view"><a href="#list-view" role="tab" data-toggle="tab"><i class="zmdi zmdi-view-list"></i></a></li>
                    </ul>
                    <!-- End List And Grid View -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="shop__grid__view__wrap another-product-style">
                <?php if(!empty($productsArr)) : ?>
                <?php foreach ($productsArr as $product): ?>
                <div role="tabpanel" id="grid-view" class="single-grid-view tab-pane fade in active clearfix">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                        <div class="product">
                            <div class="product__inner">
                                <div class="pro__thumb">
                                    <a href="#" style="height:270px; position: relative;">
                                        <?php if(!empty($product['photo'])) : ?>
                                            <img src="/web/uploads/<?= ltrim($product['photo'][0]['title']) ?>" alt="product images" style="position: absolute;top: 50%;transform: translateY(-50%);">
                                        <?php else: ?>
                                            <img src="/images/products/placeholderProduct.png" alt="product images">
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="product__hover__info">
                                    <ul class="product__action">
                                            <li><a  data-id="<?= $product['id'] ?>" data-target="#productModal" title="Quick View" class="quick-view  detail-link " href="#"><span class="ti-plus"></span></a></li>
                                        <li><a title="Add To Cart" data-id="<?= $product['id'] ?>" href="#" class="add-to-cart"><span class="ti-shopping-cart"></span></a></li>
                                        <li><a title="Wishlist" href="wishlist.html"><span class="ti-heart"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product__details">
                                <h2><a href="/product?id=<?= $product['id'] ?>"><?= $product['name'] ?></a></h2>
                                <ul class="product__price">
                                    <!--                                                    <li class="old__price">$16.00</li>-->
                                    <li class="new__price">Цена: <?= $product['price'] ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <div class="text-danger text-center" style="margin-top: 30px">В данной категории нет продуктов!</div>
                    <?php endif; ?>
                </div>
                <!-- End Single View -->
                <!-- Start Single View -->
                <div role="tabpanel" id="list-view" class="single-grid-view tab-pane fade clearfix">
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/1.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/2.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/3.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/4.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/5.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                    <!-- Start List Content-->
                    <div class="single__list__content clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                            <div class="list__thumb">
                                <a href="product-details.html">
                                    <img src="/images/products/product/6.png" alt="list images">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                            <div class="list__details__inner">
                                <h2><a href="product-details.html">Ninja Silhouette</a></h2>
                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet…</p>
                                <span class="product__price">$15.00</span>
                                <div class="shop__btn">
                                    <a class="htc__btn" href="cart.html"><span class="ti-shopping-cart"></span>Add to Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End List Content-->
                </div>
                <!-- End Single View -->
            </div>
        </div>
    </div>
</div>