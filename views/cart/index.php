<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php if(!empty($cart)) :  ?>
        <form action="cart/update" method="post">
            <div class="table-content table-responsive">
                <table>
                    <thead>
                    <tr>
                        <th class="product-thumbnail">Изображение</th>
                        <th class="product-name">Продукт</th>
                        <th class="product-price">Цена</th>
                        <th class="product-quantity">Количество</th>
                        <th class="product-subtotal">Всего</th>
                        <th class="product-remove">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($cart as $item) : ?>
                    <?php if($item == $cart['total']) {
                            continue;
                        } ?>
                    <tr>
                        <input type="hidden" name="cart_id[]" value="<?= $item['cart_id'] ?>">
                        <td class="product-thumbnail"><a href="#"><img src="<?= $item['photo'] ? 'web/uploads/'.$item['photo']['title'] : '/images/products/placeholderProduct.png' ?>" alt="product img"></a></td>
                        <td class="product-name"><a href="#"><?= $item['product']['name'] ?></a></td>
                        <td class="product-price"><span class="amount"><?= $item['product']['price'] ?></span></td>
                        <td class="product-quantity"><input type="number" name="quantity[]" min="1" value="<?= $item['Quantity'] ?>" class="cart-number"></td>
                        <td class="product-subtotal"><?= $item['productTotalPrice']?></td>
                        <td class="product-remove"><a href="#" data-id="<?= $item['cart_id'] ?>" class="remove-cart">X</a></td>
                    </tr>
                    <?php endforeach; ?>
                    <div id="test"></div>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <div class="buttons-cart">
                        <input type="submit" value="Обновить корзину">
                        <a href="/">Продолжить покупки</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="cart_totals">
                        <h2>Cart Totals</h2>
                        <table>
                            <tbody>
                            <tr class="order-total">
                                <br>
                                <th>Общая стоимость</th>
                                <td>
                                    <strong><span class="amount"><?= $cart['total'] ?></span></strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="wc-proceed-to-checkout">
                            <a href="/checkout">Перейти к оформлению заказа</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php else : ?>
        <h2 class="text-primary text-center ">Вы пока не добавиль товар!</h2>
        <?php endif; ?>
    </div>
</div>