<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php if(!empty($errorLogin)): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $errorLogin ?>
                </div>
            <?php endif; ?>
            <?php $errorLogin = ''?>
            <ul class="login__register__menu" role="tablist">
                <li role="presentation" class="login active"><a href="#login" role="tab" data-toggle="tab" aria-expanded="false">Login</a></li>
                <li role="presentation" class="register "><a href="#register" role="tab" data-toggle="tab" aria-expanded="true">Register</a></li>
            </ul>
        </div>
    </div>
    <!-- Start Login Register Content -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="htc__login__register__wrap">
                <!-- Start Single Content -->
                <div id="register" role="tabpanel" class="single__tabs__panel tab-pane fade ">
                    <?php $form = ActiveForm::begin([ 'class' => 'login', 'action' => '/auth/register']) ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => "User Name*"])->label(false) ?>
                    <?= $form->field($model, 'email')->textInput(['type' => 'email','placeholder' => "Email*"])->label(false) ?>
                    <?= $form->field($model, 'password')->textInput(['type' => 'password', 'placeholder' => "Password*"])->label(false) ?>

                    <div class="tabs__checkbox">
                        <input type="checkbox">
                        <span> Remember me</span>
                    </div>
                    <div class="htc__login__btn">
                        <?= Html::submitButton('Регистрация', ['class' => '']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="htc__social__connect">
                        <h2>Or Login With</h2>
                        <ul class="htc__soaial__list">
                            <li><a class="bg--twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a class="bg--instagram" href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                            <li><a class="bg--facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a class="bg--googleplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Single Content -->
                <!-- Start Single Content -->
                <div id="login" role="tabpanel" class="single__tabs__panel tab-pane fade active in">
                    <?php $form = ActiveForm::begin([ 'class' => 'login', 'action' => '/auth/login']) ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => "User Name*"])->label(false) ?>
                    <?= $form->field($model, 'password')->textInput(['type' => 'password', 'placeholder' => "Password*"])->label(false) ?>

                    <div class="tabs__checkbox">
                        <input type="checkbox">
                        <span> Remember me</span>
                        <span class="forget"><a href="#">Forget Pasword?</a></span>
                    </div>
                    <div class="htc__login__btn mt--30">
                        <?= Html::submitButton('Вход', ['class' => '']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="htc__social__connect">
                        <h2>Or Login With</h2>
                        <ul class="htc__soaial__list">
                            <li><a class="bg--twitter" href="#"><i class="zmdi zmdi-twitter"></i></a></li>

                            <li><a class="bg--instagram" href="#"><i class="zmdi zmdi-instagram"></i></a></li>

                            <li><a class="bg--facebook" href="#"><i class="zmdi zmdi-facebook"></i></a></li>

                            <li><a class="bg--googleplus" href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Single Content -->
            </div>
        </div>
    </div>
    <!-- End Login Register Content -->
</div>