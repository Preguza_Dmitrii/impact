<div class="row">
    <div class="col-md-8 col-lg-8">
        <div class="ckeckout-left-sidebar">
            <?php if (!empty($_SESSION['checkError'])): ?>
            <div class="alert alert-danger" role="alert">
                <?= $_SESSION['checkError'] ?>
            </div>
            <?php endif; ?>
            <?php $_SESSION['checkError'] = '' ?>
            <!-- Start Checkbox Area -->
            <form class="checkout-form" action="/checkout/check" method="post">
                <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                <h2 class="section-title-3">Оформление заказа</h2>
                <div class="checkout-form-inner">
                    <div class="single-checkout-box">
                        <input type="text" name="firstName" placeholder="Имя*" required>
                        <input type="text" name="lastName" placeholder="Фамилия*" required>
                    </div>
                    <div class="single-checkout-box">
                        <input type="email" name="email" placeholder="Emil*" required>
                        <input type="text" name="phone" placeholder="Номер телефона*" required>
                    </div>
                    <div class="single-checkout-box">
                        <input type="text" name="address" placeholder="Адрес*" style="width: 100%" required>
                    </div>
                    <div class="single-checkout-box">
                        <textarea name="message" placeholder="Сообщение*"></textarea>
                    </div>
<!--                    <div class="single-checkout-box select-option mt--40">-->
<!--                        <select>-->
<!--                            <option>Молдова</option>-->
<!--                            <option>Австрия</option>-->
<!--                            <option>Белоруссия</option>-->
<!--                            <option>Бельгия</option>-->
<!--                        </select>-->
<!--                        <input type="text" placeholder="Company Name*">-->
<!--                    </div>-->
<!--                    <div class="single-checkout-box">-->
<!--                        <input type="email" placeholder="State*">-->
<!--                        <input type="text" placeholder="Zip Code*">-->
<!--                        -->
<!--                    </div>-->
<!--                    <div class="single-checkout-box checkbox">-->
<!--                        <input id="remind-me" type="checkbox">-->
<!--                        <label for="remind-me"><span></span>Create a Account ?</label>-->
<!--                    </div>-->
                    <p class="checkout-info">Опрлата наличными по прибытию тавара</p>
                </div>
                <div class="checkout-btn">
                    <button type="submit" class="checkout-submit ts-btn btn-light btn-large hover-theme" >ОТПРАВИТЬ ЗАКАЗ</button>
<!--                    <a class="ts-btn btn-light btn-large hover-theme" href="#">CONFIRM & BUY NOW</a>-->
                </div>
            </form>
            <!-- End Checkbox Area -->
            <!-- Start Payment Box -->
<!--            <div class="payment-form">-->
<!--                <h2 class="section-title-3">payment details</h2>-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur kgjhyt</p>-->
<!--                <div class="payment-form-inner">-->
<!--                    <div class="single-checkout-box">-->
<!--                        <input type="text" placeholder="Name on Card*">-->
<!--                        <input type="text" placeholder="Card Number*">-->
<!--                    </div>-->
<!--                    <div class="single-checkout-box select-option">-->
<!--                        <select>-->
<!--                            <option>Date*</option>-->
<!--                            <option>Date</option>-->
<!--                            <option>Date</option>-->
<!--                            <option>Date</option>-->
<!--                            <option>Date</option>-->
<!--                        </select>-->
<!--                        <input type="text" placeholder="Security Code*">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!-- End Payment Box -->
            <!-- Start Payment Way -->
<!--            <div class="our-payment-sestem">-->
<!--                <h2 class="section-title-3">We  Accept :</h2>-->
<!--                <ul class="payment-menu">-->
<!--                    <li><a href="#"><img src="/images/products/payment/1.jpg" alt="payment-img"></a></li>-->
<!--                    <li><a href="#"><img src="/images/products/payment/2.jpg" alt="payment-img"></a></li>-->
<!--                    <li><a href="#"><img src="/images/products/payment/3.jpg" alt="payment-img"></a></li>-->
<!--                    <li><a href="#"><img src="/images/products/payment/4.jpg" alt="payment-img"></a></li>-->
<!--                    <li><a href="#"><img src="/images/products/payment/5.jpg" alt="payment-img"></a></li>-->
<!--                </ul>-->

<!--            </div>-->
            <!-- End Payment Way -->
<!--        </div>-->
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="checkout-right-sidebar">
<!--            <div class="our-important-note">-->
<!--                <h2 class="section-title-3">Note :</h2>-->
<!--                <p class="note-desc">Lorem ipsum dolor sit amet, consectetur adipisici elit, sed do eiusmod tempor incididunt ut laborekf et dolore magna aliqua.</p>-->
<!--                <ul class="important-note">-->
<!--                    <li><a href="#"><i class="zmdi zmdi-caret-right-circle"></i>Lorem ipsum dolor sit amet, consectetur nipabali</a></li>-->
<!--                    <li><a href="#"><i class="zmdi zmdi-caret-right-circle"></i>Lorem ipsum dolor sit amet</a></li>-->
<!--                    <li><a href="#"><i class="zmdi zmdi-caret-right-circle"></i>Lorem ipsum dolor sit amet, consectetur nipabali</a></li>-->
<!--                    <li><a href="#"><i class="zmdi zmdi-caret-right-circle"></i>Lorem ipsum dolor sit amet, consectetur nipabali</a></li>-->
<!--                    <li><a href="#"><i class="zmdi zmdi-caret-right-circle"></i>Lorem ipsum dolor sit amet</a></li>-->
<!--                </ul>-->
<!--            </div>-->
            <div class="puick-contact-area mt--60">
                <h2 class="section-title-3">Quick Contract</h2>
                <a href="tel:+373(60)-849-464">+373(60)-849-464</a>
            </div>
        </div>
    </div>
</div>