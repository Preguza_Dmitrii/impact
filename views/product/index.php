<div class="scroll-active">
    <div class="row">
        <div class="col-md-7 col-lg-7 col-sm-5 col-xs-12">
            <div class="product__details__container product-details-5">
                <?php foreach ($products[0]['photo'] as $product):?>
                <div class="scroll-single-product mb--30">
                    <img src="/web/uploads/<?= $product['title'] ?>" alt="full-image">
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="sidebar-active col-md-5 col-lg-5 col-sm-7 col-xs-12 xmt-30">
            <div class="htc__product__details__inner ">
                <?php foreach ($products as $product): ?>
                <div class="pro__detl__title">
                    <h2><?= $product['name'] ?></h2>
                </div>
                <div class="pro__details">
                    <p><?= $product['info'] ?></p>
                </div>
                <ul class="pro__dtl__prize">
                    <li><h3 style="font-size: 24px; display: inline-block"><b>Цена: </b> </h3> <?= $product['price'] ?></li>
                </ul>
                <?php endforeach; ?>
                <a href="/"></a>
            </div>
        </div>
    </div>
</div>