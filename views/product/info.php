
<div class="modal-product">
    <!-- Start product images -->
    <div class="product-images">
        <div class="main-image images">
            <img alt="big images" src="<?= $photo[0]['file_name'] ? '/web/uploads/' . $photo[0]['file_name'] : '/images/products/placeholderProduct.png' ?>" >
        </div>
    </div>
    <!-- end product images -->
    <div class="product-info" style="min-width: 40%">
        <h1><?= $product->name ?></h1>
        <div class="price-box-3">
            <div class="s-price-box">
                <span class="new-price"><?= $product->price ?></span>
<!--                <span class="old-price">$45.00</span>-->
            </div>
        </div>
        <div class="quick-desc">
            <?= $product->info ?>
        </div>
    </div><!-- .product-info -->
</div>