<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<div class="container-xl category-box">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title p-3">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Изменить <b>Категорию</b></h2>
                    </div>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'update-categories']); ?>
<!--                <div class="form-group d-flex">-->
                    <?= $form->field($model, 'name')->textInput(['class' => 'categories-add mr-3 ps-2 mb-2  update-categories-btn', 'value' => "$name"])->label(false); ?>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success categories-btn-add']); ?>
                    <a href="/admin/categories" class="btn btn-danger ms-3 categories-btn-add">Отмена</a>
<!--                </div>-->
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>


