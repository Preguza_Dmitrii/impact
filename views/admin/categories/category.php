<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

?>


<div class="container-xl category-box">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title p-3">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Управление <b>Категориями</b></h2>
                    </div>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'category-form', 'action' => '/admin/categories/create', 'options' => ['class' => 'row']]) ?>
                <div class="form-group mb-3 d-flex">
                <?= $form->field($category, 'name')->textInput(['class' => 'categories-add mr-3 ps-2 admin-add-category-name', 'style' => 'height:42px; ' , 'placeholder' => 'Имя Категории', 'required'])->label(false) ?>
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-success categories-btn-add', 'id' => 'admin-add-category']) ?>
                </div>
                <?php ActiveForm::end() ?>
                <div class="admin-add-plan-errors"></div>
                <!--<form class="row" action="/admin/categories/create" method="post">-->
<!--                    <input type="hidden" name="_csrf" value="--><?//=Yii::$app->request->getCsrfToken()?><!--" />-->
<!--                    <input type="text" name="name" class="categories-add mr-3" placeholder="Имя Категории" required/>-->
<!--                    <button  class="btn btn-success categories-btn-add">Добавить</button>-->
<!--                </form>-->
            </div>
            <?php if(!empty($categories) && !empty($pages)) : ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Имя Категории</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="category-table">

                <?php foreach ($categories as $value) : ?>

                    <tr>
                        <td class="col-11"><?= $value->name ?></td>
                        <td class="col-1 border-start border-black">
                            <a href="categories/update/<?= $value->id ?>" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="" data-original-title="Edit"></i></a>
                            <a href="categories/delete/<?= $value->id ?>" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
                <div class="clearfix">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                        'options' => ['class' => 'pagination'],
                        'linkOptions' => ['class' => 'page-link']
                    ]); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>