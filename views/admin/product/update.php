<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<div class="container-xl category-box">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title p-3">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Изменить <b>Продукт</b></h2>
                    </div>
                </div>
                <?php  $form = ActiveForm::begin([ 'id' => 'product-form']) ?>
                <div class="mb-3 add-categories">
                    <?php if (!empty($categories)) : ?>
                        <select class="js-example-basic-single" name="categoriesId[]" multiple="multiple">
                            <?php foreach ($categories as $value) : ?>
                            <?php $selected = in_array($value->id, $productId); ?>
                                <option value="<?= $value->id ?>" <?= ($selected ? "selected" : ""); ?>><?= $value->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                </div>
                <div class="form-group d-flex mb-2">
                    <?= $form->field($model, 'id')->textInput([ 'class' => 'categories-add mr-3 ps-2 update-product', 'value' => "$product->id", 'type' => 'hidden'])->label(false) ?>
                    <?= $form->field($model, 'name')->textInput([ 'class' => 'categories-add mr-3 ps-2 update-product', 'value' => " $product->name "])->label(false) ?>
                    <?= $form->field($model, 'info')->textInput(['class' => 'categories-add mr-3 ps-2 update-product', 'value' => "$product->info"])->label(false) ?>
                    <?= $form->field($model, 'price')->textInput([ 'class' => 'categories-add mr-3 ps-2 add-product-price update-product', 'value' => "$product->price"])->label(false) ?>
                </div>
                <div class="form-group d-flex">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success categories-btn-add']) ?>
                    <a href="/admin/product" class="btn btn-danger ms-3 categories-btn-add">Отмена</a>
                </div>
                <?php ActiveForm::end(); ?>

                <?php $form = ActiveForm::begin(['action' => '/admin/product/add-photo']) ?>
                <h4 class="mt-5">Добавить Фото</h4>
                <div class="form-group d-flex">
                    <?= $form->field($photo, 'product_id')->textInput([ 'value' => "$product->id", 'type' => 'hidden'])->label(false) ?>
                    <?= $form->field($photo, 'file_name[]')->fileInput(['class' => 'mb-3 mt-2', 'multiple' => 'multiple'])->label(false) ?>
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success categories-btn-add']) ?>
                </div>
                <?php ActiveForm::end(); ?>

                <div class="gallery mt-5 row">
                    <?php foreach ($photos as $photo): ?>
                        <?php  $form = ActiveForm::begin(['action' => '/admin/product/delete-photo']) ?>
                        <?= $form->field($photo, 'id')->textInput([ 'value' => "$photo->id", 'type' => 'hidden'])->label(false) ?>
                        <?= $form->field($photo, 'title')->textInput([ 'type' => 'hidden', 'value' => " $photo->title" ])->label(false) ?>
                        <img src="/web/uploads/<?= $photo->title ?>" alt="Foto" style=" width: 200px;; margin: 0 auto;">
                            <?= Html::submitButton('Удалить', ['class' => 'btn btn-danger categories-btn-add ']) ?>
                        <?php ActiveForm::end(); ?>
                    </form>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>


