<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="container-xl category-box">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title p-3">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Управление <b>Продуктами</b></h2>
                    </div>
                </div>

                <?php $form = ActiveForm::begin(['id' => 'product-form', 'options' => ['class' => 'row', 'enctype' => 'multipart/form-data']]) ?>
                <div class="mb-3 add-categories">
                    <?php if (!empty($categories)) : ?>
                        <select id="add-product-category" class="js-example-basic-single" name="categoriesId[]" multiple="multiple">
                            <?php foreach ($categories as $value) : ?>
                                <option value="<?= $value->id ?>"><?= $value->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                </div>
                <div class="form-group mb-3">
                    <?= $form->field($model, 'id')->textInput(['class' => 'categories-add mr-3 add-product-name admin-add-product-id', 'type' => 'hidden'])->label(false) ?>
                    <?= $form->field($model, 'name')->textInput(['class' => 'categories-add mr-3 add-product-name admin-add-product-name', 'placeholder' => 'Имя Продукта', 'required'])->label(false) ?>
                    <?= $form->field($model, 'info')->textInput(['class' => 'categories-add mr-3 admin-add-product-info', 'placeholder' => 'Краткое Описание'])->label(false) ?>
                    <?= $form->field($model, 'price')->textInput(['class' => 'categories-add mr-3 add-product-price admin-add-product-price', 'placeholder' => 'Цена', 'required'])->label(false) ?>
                </div>
                <?= $form->field($photo, 'file_name[]')->fileInput(['multiple' => 'multiple', 'class' => 'admin-add-product-file_name'])->label(false) ?>
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success categories-btn-add ms-3 mt-3', 'id' => 'admin-add-product']) ?>
                <?php ActiveForm::end() ?>

                <div class="admin-add-plan-errors"></div>
            </div>

            <?php if(!empty($arr) ) : ?>
            <table class="table table-striped table-hover product-table">
                <thead>
                <tr>
                    <th>Имя Продукта</th>
                    <th class="border-start border-black">Категория</th>
                    <th class="border-start border-black">Краткое Описание</th>
                    <th class="border-start border-black">Фото Товара</th>
                    <th class="border-start border-black">Цена</th>
                    <th class="border-start border-black"></th>
                </tr>
                </thead>
                <tbody id="product-table" class="product-table">
                <?php foreach ($arr as $value) : ?>
                <tr>
                    <td><?= $value['product']->name ?></td>
                    <td class="border-start border-black">
                        <?php
                        foreach ($value['category'] as $item) {
                                echo $item->category->name.', ';
                        }
                        ?>
                    </td class="border-start border-black">
                    <td class="border-start border-black"><?= $value['product']->info ?></td>
                    <td class="border-start border-black">
                        <ul class="product-photo">
                            <?php foreach ($value['photo'] as $photo): ?>
                                    <li><?= $photo->file_name ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </td>
                    <td class="border-start border-black"><?= $value['product']->price ?></td>
                    <td class="border-start border-black">
                        <a href="product/update/<?= $value['product']->id ?>" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="" data-original-title="Edit"></i></a>
                        <a href="product/delete/<?= $value['product']->id ?>" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
                <div class="clearfix">
<!--                    --><?//= LinkPager::widget([
//                        'pagination' => $pages,
//                        'options' => ['class' => 'pagination'],
//                        'linkOptions' => ['class' => 'page-link']
//                    ]); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>